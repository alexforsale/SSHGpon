import paramiko
import time
import re
import mariadb
from config import *

def querySSH(hosts, ports):
    """Query data from SSH.
    Output should be in the form of list with 2 items.
    Example:
    ['0    online  2022-12-19 14:48:27 2022-12-19 14:22:48 LOSi/LOBi\n1    online  2022-12-05 16:18:24 2022-12-05 14:55:33 dying-gasp\n2    offline -                   -                   -\n3    offline 2022-12-09 14:53:23 2022-12-09 15:09:28 dying-gasp\n',
     '0   48575443FEAEA8A6 EG8141A5         57    -20.04/2.22  ONT test 1\n1   48575443FEACCEA6 EG8141A5         1601  -20.08/2.26  ONT_NO_DESCRIPTION\n2   48575443995BF69B -                -     -/-          ONT test 3\n3   48575443B8ADD79F HG8245H5         -     -/-          ONT test 4\n']
    """
    conn = paramiko.SSHClient()
    conn.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    out =  {}
    raw_info = {}
    info = {}
    match1 = {}
    match2 = {}
    values1 = {}
    values2 = {}
    for host in hosts:
        #conn.connect(gpon_ssh_host, gpon_ssh_port, gpon_ssh_username, gpon_ssh_password)
        conn.connect(host, gpon_ssh_port, gpon_ssh_username, gpon_ssh_password)
        commands = conn.invoke_shell()
        commands.send("scroll 512\n")
        commands.send("enable\n")
        commands.send("config\n")
        info[host] = {}
        out[host] = {}
        raw_info[host] = {}
        match1[host] = {}
        match2[host] = {}
        values1[host] = {}
        values2[host] = {}
        for port in ports:
            out[host][port] = []
            info[host][port] = ""
            raw_info[host][port] = ""
        for port in ports:
            commands.send("display ont info summary" + " " + port)
            time.sleep(0.5)
            commands.send("\n\r")
            time.sleep(0.5)
            raw_info[host][port] = commands.recv(65535)
            raw_info[host][port] = str(raw_info[host][port].decode("utf-8"))
            time.sleep(1)
            for line in raw_info[host][port].splitlines():
                info[host][port] += line.strip() + '\n'
            match1[host][port] = re.search(r"(ONT\s+Run.*\n)(.*\n){2}(\d.*\n)+", info[host][port]).group(0)
            match2[host][port] = re.search(r"(ONT\s+SN.*\n)(.*\n){2}(\d.*\n)+", info[host][port]).group(0)
            values1[host][port] = re.search("(\d+)(.*\n)+", match1[host][port]).group(0)
            out[host][port].append(values1[host][port])
            values2[host][port] = re.search("(\d+)(.*\n)+", match2[host][port]).group(0)
            out[host][port].append(values2[host][port])
            time.sleep(1)
    conn.close()
    return out

class Gpon():
    #out = ['0    online  2022-12-19 14:48:27 2022-12-19 14:22:48 LOSi/LOBi\n1    online  2022-12-05 16:18:24 2022-12-05 14:55:33 dying-gasp\n2    offline -                   -                   -\n3    offline 2022-12-09 14:53:23 2022-12-09 15:09:28 dying-gasp\n',
    #       '0   48575443FEAEA8A6 EG8141A5         57    -20.04/2.22  ONT test 1\n1   48575443FEACCEA6 EG8141A5         1601  -20.08/2.26  ONT_NO_DESCRIPTION\n2   48575443995BF69B -                -     -/-          ONT test 3\n3   48575443B8ADD79F HG8245H5         -     -/-          ONT test 4\n']
    def __init__(self, host, port, queries):
        #gpon = querySSH(port)
        #self.gpon = gpon
        self.queries = queries
        self.host = [host] * len(self.queries[0].splitlines())
        self.port = [port] * len(self.queries[0].splitlines())
        self.id = self.get_id()
        self.runstate = self.get_runstate()
        self.description = self.get_description()
        self.distance = self.get_distance()
        self.lastuptime = self.get_lastuptime()
        self.lastdowntime = self.get_lastdowntime()
        self.lastdowncause = self.get_lastdowncause()
        self.rxtx_power = self.get_rxtx_power()
        self.sn = self.get_sn()
        self.type = self.get_type()

    def get_id(self):
        # column 0
        id = []
        for x in self.queries[0].splitlines():
            id.append(x.split()[0])
        return id

    def get_runstate(self):
        # column 1
        runstate = []
        for x in self.queries[0].splitlines():
            runstate.append(x.split()[1])
        return runstate

    def get_lastuptime(self):
        # column 2: date YYYY-MM-DD
        # column 3: time HH:MM:SS
        # or column 2: '-'
        lastuptime = []
        for x in self.queries[0].splitlines():
            if re.search("\d{4}-\d{2}-\d{2}",
                         x.split()[2]) and re.search("\d{2}:\d{2}:\d{2}",
                                                     x.split()[3]):
                lastuptime.append(x.split()[2] + " " + x.split()[3])
            elif x.split()[2] == "-":
                lastuptime.append(x.split()[2])
        return lastuptime

    def get_lastdowntime(self):
        # if lastuptime is not -: column 4 : date YYYY-MM-DD column 5 time HH:MM:SS
        # if lastuptime is -: column 3
        lastdowntime = []
        for x in self.queries[0].splitlines():
            # if lastuptime is not '-'
            if x.split()[2] != '-':
                if x.split()[4] == '-':
                    # lastdowntime is '-'
                    lastdowntime.append(x.split()[4])
                elif re.search("\d{4}-\d{2}-\d{2}",
                               x.split()[4]) and re.search("\d{2}:\d{2}:\d{2}",
                                                           x.split()[5]):
                    # lastdowntime date + time
                    # column 4 + column 5
                    lastdowntime.append(x.split()[4] + " " + x.split()[5])
            elif x.split()[2] == '-':
                lastdowntime.append(x.split()[3])
        return lastdowntime

    def get_lastdowncause(self):
        lastdowncause = []
        for x in self.queries[0].splitlines():
            lastdowncause.append(x.split()[-1])
        return lastdowncause

    # querySSH() output 1
    # column 0 is ont id
    def get_sn(self):
        # column 1
        sn = []
        for x in self.queries[1].splitlines():
            sn.append(x.split()[1])
        return sn

    def get_type(self):
        # column 2
        type_ = []
        for x in self.queries[1].splitlines():
            type_.append(x.split()[2])
        return type_

    def get_distance(self):
        # column 3
        distance = []
        for x in self.queries[1].splitlines():
            distance.append(x.split()[3])
        return distance

    def get_rxtx_power(self):
        # column 4
        rxtx_power = []
        for x in self.queries[1].splitlines():
            rxtx_power.append(x.split()[4])
        return rxtx_power

    def get_description(self):
        description = []
        for x in self.queries[1].splitlines():
            if len(x.split()) <= 6:
                description.append(x.split()[5])
            else:
                desc = " ".join(x.split()[5:])
                description.append(desc)
        return description

db_config = {
    'host': gpon_db_host,
    'port': int(gpon_db_port),
    'user': gpon_db_username,
    'password': gpon_db_password,
    'database': gpon_db
}

db_table = gpon_db_table

def dropTable():
    """Drop table from database."""
    # connect to mariadb
    try:
        conn = mariadb.connect(**db_config)
        cur = conn.cursor()
        cur.execute(f"DROP TABLE IF EXISTS `{db_table}`")
        mariadb.connect(**db_config).close()
        #print("Connection closed.")
    except mariadb.Error as e:
        print(f"{e}")

def createTable():
    """Create table in database."""
    # connect to mariadb
    #if len(table_data) == 0:
    #    query_board(cf_host, cf_port, cf_username, cf_password, cf_gpon_port)

    try:
        conn = mariadb.connect(**db_config)
        cur = conn.cursor()
        cur.execute(f"CREATE TABLE `{db_table}` (gpon_host VARCHAR(255), gpon_port VARCHAR(255), ont_id INT, run_state VARCHAR(255), last_uptime VARCHAR(255), last_downtime VARCHAR(255), last_downcause VARCHAR(255), sn VARCHAR(255) PRIMARY KEY, type VARCHAR(255), distance VARCHAR(255), rx_tx_power VARCHAR(255), description VARCHAR(255))")
        mariadb.connect(**db_config).close()
        #print("Connection closed.")
    except mariadb.Error as e:
        print(f"{e}")

def insertTable(port, gpons):
    """Put data from HQuery to the database table."""
    # connect to mariadb
    try:
        conn = mariadb.connect(**db_config)
        cur = conn.cursor()
        for x in gpons:
            for y in gpons[x]:
                for z in range(len(gpons[x][y].port)):
                    gpon_host = gpons[x][y].host[z]
                    gpon_port = gpons[x][y].port[z]
                    ont_id = gpons[x][y].id[z]
                    run_state = gpons[x][y].runstate[z]
                    last_uptime = gpons[x][y].lastuptime[z]
                    last_downtime = gpons[x][y].lastdowntime[z]
                    last_downcause = gpons[x][y].lastdowncause[z]
                    sn = gpons[x][y].sn[z]
                    _type = gpons[x][y].type[z]
                    distance = gpons[x][y].distance[z]
                    rx_tx_power = gpons[x][y].rxtx_power[z]
                    description = gpons[x][y].description[z]
                    print(f"""INSERT INTO `{db_table}` (gpon_host, gpon_port, ont_id, run_state, last_uptime, last_downtime, last_downcause, sn, type, distance, rx_tx_power, description) VALUES ("{gpon_host}", "{gpon_port}", "{ont_id}", "{run_state}", "{last_uptime}", "{last_downtime}", "{last_downcause}", "{sn}", "{_type}", "{distance}", "{rx_tx_power}", "{description}")""")
                    cur.execute(f"""INSERT INTO `{db_table}` (gpon_host, gpon_port, ont_id, run_state, last_uptime, last_downtime, last_downcause, sn, type, distance, rx_tx_power, description) VALUES ("{gpon_host}", "{gpon_port}", "{ont_id}", "{run_state}", "{last_uptime}", "{last_downtime}", "{last_downcause}", "{sn}", "{_type}", "{distance}", "{rx_tx_power}", "{description}")""")
        conn.commit()
        conn.close()
    except mariadb.Error as e:
        print(f"{e}")

if __name__ == '__main__':
    dump = querySSH(gpon_ssh_host, gpon_ports)
    gpons = {} # type: dict
    dropTable()
    createTable()
    for host in gpon_ssh_host:
        gpons[host] = {}
        for port in gpon_ports:
            print("port: ", port)
            print(f"dump[{port}]: ",dump[host][port])
            gpons[host][port] = Gpon(host, port, dump[host][port])
    insertTable(gpon_ports, gpons)
