from dotenv import dotenv_values

gpon_ssh_host = dotenv_values('.env')['gpon_ssh_host'].split(",")
gpon_ssh_port = dotenv_values('.env')['gpon_ssh_port']
gpon_ssh_username = dotenv_values('.env')['gpon_ssh_username']
gpon_ssh_password = dotenv_values('.env')['gpon_ssh_password']
gpon_db = dotenv_values('.env')['gpon_db']
gpon_db_host = dotenv_values('.env')['gpon_db_host']
gpon_db_port = dotenv_values('.env')['gpon_db_port']
gpon_db_username = dotenv_values('.env')['gpon_db_username']
gpon_db_password = dotenv_values('.env')['gpon_db_password']
gpon_db_table = dotenv_values('.env')['gpon_db_table']
gpon_ports = dotenv_values(".env")['gpon_port'].split(",")
